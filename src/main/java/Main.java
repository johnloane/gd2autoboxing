import java.util.ArrayList;

class IntClass
{
    private int myValue;

    public IntClass(int myValue)
    {
        this.myValue = myValue;
    }

    public int getMyValue()
    {
        return myValue;
    }

    public void setMyValue(int myValue)
    {
        this.myValue = myValue;
    }
}

public class Main
{
    public static void main(String[] args)
    {
        String[] stringArray = new String[10];
        int[] integerArray = new int[10];

        ArrayList<String> stringArrayList = new ArrayList<String>();
        stringArrayList.add("Banana");

        //ArrayList<int> integerArrayList = new ArrayList<int>;
//        ArrayList<IntClass> integerArrayList = new ArrayList<IntClass>();
//        integerArrayList.add(new IntClass(5));

        Integer integer = Integer.valueOf(7);
        Double doubleValue = Double.valueOf(7.0);

        ArrayList<Integer> integerArrayList = new ArrayList<Integer>();
        for(int i = 0; i <=10; ++i)
        {
            integerArrayList.add(Integer.valueOf(i));
        }

        for(int i = 0; i <=10; ++i)
        {
            System.out.println(i + "---> " + integerArrayList.get(i).intValue());
        }

        ArrayList<Double> myDoubleValues = new ArrayList<Double>();
        for(double db = 0.0; db <= 10.0; db+=0.5)
        {
            myDoubleValues.add(db);
        }

        for(int i=0; i < myDoubleValues.size(); ++i)
        {
            double value = myDoubleValues.get(i);
            System.out.println(i + " ---> " + value);
        }

    }
}
